Django==2.0.3
psycopg2==2.7.7
psycopg2-binary
setuptools==40.6.3
gunicorn
django-heroku
