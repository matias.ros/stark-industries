**Stark Industries -  Admin**: La aplicación será utilizada para la administracion de los recursos económicos y financieros.

**Ramas:** 
    - Master: Utilizada para el deploy en producción.
    - Developer: Todas las pruebas y cambios se realizan aquí, y se deployará en desarrollo.

**Fases para el despliegue de la aplicación**

    CHECKING: Compilación del código fuente que implica la revisión de la sintaxis de dicho código. Igualmente, se chequea la sintaxis para asegurarse de tener el mínimo de errores.

    DEVELOPMENT: los cambios de la rama developer son los utilizados para deployar la app "starkind" en el servidor de desarrollo, visitelo en https://starkind.herokuapp.com.

    HOMOLOGACION: Las pruebas unitarias son ejecutadas en este momento. Una vez que todas ellas fueron terminadas con éxito, estará en condiciones para que la aplicacion sea deployada en el entorno de produccion.

    PRODUCTION: los cambios de la rama master son los utilizados para deployar la app "starkind-prod" en el servidor de producción, visitelo en https://starkind.prod.herokuapp.com.