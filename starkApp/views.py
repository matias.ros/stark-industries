from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.http import HttpResponse


def loginmati(request):
    if request.user.id is not None:
        return redirect('principal')

    if request.method == "POST":
        data = request.POST
        if 'name' in data and 'password' in data:
            user = authenticate(
                username=data['name'],
                password=data['password']
            )
            if user is not None:
                login(request, user)
                return redirect('principal')

    return render(request, 'login.html')

@login_required
def cerrarsesion(request):
    logout(request)
    return redirect('login')
    
def principal(request):
    return render(request, 'home.html')


def redirigir(request):
    return redirect('principal')

