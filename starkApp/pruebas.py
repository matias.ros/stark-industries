from django.test import TestCase
from django.contrib.auth.models import User


class Test(TestCase):

    def setUp(self):
        self.user = User.objects.create_superuser(
            username='matiasros',
            email='matirosper@gmail.com',
            password='4054271'
        )

    def test_uni(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)


    def test_iniciosesion(self):
        response = self.client.post("/login/", data={
            "name": "matiasros",
            "password": "4054271"
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/")

