from django.contrib import admin
from django.urls import path
from django.urls import include, path
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from starkApp.views import loginmati,cerrarsesion,principal,redirigir
from django.urls import re_path

urlpatterns = [
    path('login',loginmati,name="login"),
    path('cerrarsesion',cerrarsesion, name='cerrarsesion'),
    path('',principal,name="principal"),
    re_path(r'.+',redirigir)
]